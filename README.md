# bazaar-web-crawler
This projects is going to extract all of the android applications from https://cafebazaar.ir/?l=en and store the data a in folder.  
Each application's data will be stored in a separate file.  
You can use this data for some analytics algorithms.  
You also can change this code to use it in a way that you like for crawling a website that you like.  
THANK YOU for seeing this project :)  
## Screenshots
![](Bazzar Crawler.PNG)
